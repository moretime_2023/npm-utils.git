import { defineConfig } from 'vitepress'
import typedocSidebar from '../api/typedoc-sidebar.json'

// https://vitepress.dev/reference/site-config
export default defineConfig({
  title: "@Moretime",
  base: '/doc/@moretime.util/',
  outDir: '../@moretime.util',
  themeConfig: {
    logo: 'https://www.moretime.fun/assets/logo/logo.png',
    logoLink: 'https://www.moretime.fun/doc/@moretime.util',
    sidebar: [{text: 'Apis', items: typedocSidebar}],
    search: {
      provider: 'local'
    },
    socialLinks: [
      { icon: 'npm', link: 'https://www.npmjs.com/package/@moretime/util' }
    ],
  }
})
