// vite.config.js
import { readdirSync } from 'fs';
import { defineConfig } from 'vite';
import { node } from '@liuli-util/vite-plugin-node/dist/index.js'

const entry = readdirSync('./src').map(k => `./src/${k}`)

/** @type {import "vite".UserConfig} */
export default defineConfig({
  build: {
    lib: {
      entry,
    },
  },
  plugins: [node({entry})]
});
