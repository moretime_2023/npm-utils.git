# 闲暇·TypeScript工具包

## 安装
```bash
pnpm add @moretime/util -S
npm install @moretime/util -S
yarn add @moretime/util -S

```

## 使用
尽量避免使用全局变量，尽量使用模块化，避免全局污染

```
import { copyText } from '@moretime/util/browser'

copyText('hello world');
```

## License

MIT