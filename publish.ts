/** 发布npm包 */
import { spawnSync } from "child_process";
import { cpSync } from 'fs'

const path = process.argv[2]

if (path) {
  cpSync('package.json', `${path}/package.json`, { recursive: true, force: true })
  cpSync('README.md', `${path}/README.md`, { recursive: true, force: true })

  spawnSync(`cd ${path} && npm publish`, { shell: true, stdio: 'inherit' })
} else spawnSync(`npm publish`)

console.log('发布成功!')
