const playConf = {
  /** 列表循环播放 */
  loop: true,
  /** 自动播放下一曲 */
  autoNext: true,
};

/** 音乐控制器
 * @class MusicCtrl
 */
class MusicCtrl {
  /** 播放列表 */
  playList: string[] = [];
  /** 播放状态 */
  playState: 'stop' | 'play' | 'pause' = 'stop';
/** 播放配置 */
  playConf = playConf;

  /** 当前播放序号 */
  currentIdx: number = 0;
  /** audio标签 */
  audioNode!: HTMLAudioElement;

  /** 注册音频播放器
   * @param {string[]} playList 播放列表
   * @param {Partial<typeof this.playConf>} [conf] 配置
   *
   * @example const musicCtrl = new MusicCtrl(['song1.mp3', 'song2.mp3'], { loop: false });
   */
  constructor(playList: string[], conf: Partial<typeof playConf> = {}) {
    this.playList = playList;
    this.playConf = { ...this.playConf, ...conf };

    const audio = document.createElement('audio');
    document.body.appendChild(audio);
    this.audioNode = audio;
    this.currentIdx = 0;
  }

  /** 开始播放当前歌曲
   * @example musicCtrl.play();
   */
  play() {
    if (this.playList.length) {
      this.audioNode.src = this.playList[this.currentIdx];
      this.audioNode.onended = () => {
        if (this.playConf.autoNext) this.next();
      };
      this.audioNode.play();
      this.playState = 'play';
    }
  }

  /** 播放下一曲
   * @example musicCtrl.next();
   */
  next() {
    this.currentIdx++;
    if (this.currentIdx >= this.playList.length) {
      if (this.playConf.loop) {
        this.currentIdx = 0;
      }
    }

    this.play();
  }

  /** 暂停播放
   * @example musicCtrl.pause();
   */
  pause() {
    this.playState = 'pause';
    this.audioNode.pause();
  }

  /** 停止播放并重置播放状态
   * @example musicCtrl.stop();
   */
  stop() {
    if (this.playState !== 'stop') {
      this.audioNode.pause();
      this.currentIdx = 0;
      this.playState = 'stop';
    }
  }
}

export default MusicCtrl;