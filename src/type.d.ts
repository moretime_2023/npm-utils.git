interface Window {
  showSaveFilePicker?(option: {
    suggestedName: string,
    types: any[],
  }): Promise<any>;
}