export default (type: 'localStorage'|'sessionStorage' = 'localStorage') => ({
  /** 设置缓存
   * @param key
   * @param value
   * @param time 有效期，单位为小时, -1为不清空, 默认为24
   */
  set(key: string, value: any, time = 24) {
    value = JSON.stringify({
      tmp: time === -1 ? -1 : Date.now() + time * 60 * 60 * 1000,
      data: value
    })

    window[type].setItem(key, value)
  },
  /** 获取缓存
   * @param key
   */
  get(key: string) {
    const value = window[type].getItem(key)

    if (!value) return null
    else {
      const {tmp, data} = JSON.parse(value)

      if (tmp === -1) return data
      else if (Date.now() > tmp) {
        this.del(key)
        return null
      } else return data
    }
  },
  /** 删除缓存
   * @param key
   */
  del(key: string) {
    window[type].removeItem(key)
  },
  /** 清空缓存 */
  clear() {
    window[type].clear()
  }
})
