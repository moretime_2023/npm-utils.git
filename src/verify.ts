/** 用于验证手机号码。仅支持中国内地手机号，格式为13x-19x开头的11位数字。 */
export const RegExp_mobile = /^1[3-9]\d{9}$/;
/** 用于验证电子邮件地址。支持包含字母、数字、下划线、连字符的电子邮件地址，需包含域名。 */
export const RegExp_email = /^[a-zA-Z0-9_-]+@[a-zA-Z0-9_-]+(\.[a-zA-Z0-9_-]+)+$/;
/** 用于验证密码强度。要求密码必须包含至少一个大写字母、一个小写字母和一个数字，长度至少8个字符。 */
export const RegExp_password = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$/;
/** 用于验证用户名。支持包含字母和数字的用户名，长度在4到16个字符之间。 */
export const RegExp_username = /^[a-zA-Z0-9]{4,16}$/;
/** 用于验证中文姓名。支持2到16个汉字的姓名。 */
export const RegExp_cn = /^[\u4e00-\u9fa5]{2,16}$/;

/** 类型判断
 * @param payload 判断的值
 * @param type 判断的类型 支持function\date\blob\array\object\string等判断
 */
export const typeIs = <T>(payload: T, type: string): T|false => {
  const verifyResult = Object.prototype.toString.call(payload).toLocaleLowerCase().includes(`object ${type.toLocaleLowerCase()}`)

  return verifyResult ? payload : false;
}
