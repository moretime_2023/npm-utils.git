/** 设置千分符
 * @param num 数值
 * @example thousandSeparator(123456789) // 123,456,789
 */
export function thousandSeparator(num: number) {
  const [i, f] = (num || 0).toString().split('.')
  return `${i.replace(/\B(?=(\d{3})+(?!\d))/g, ",")}${f ? `.${f}` : ''}`;
};

/** fixed number
 * @param num number
 * @param len number fixed length
 * @example fixedNumber(1.23456, 2) // 1.23
 */
export const fixedNumber = (num: number, len = 2) => Number(num.toFixed(len))