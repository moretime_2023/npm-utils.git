/// <reference types="node" />
/// <reference types="node" />
import { readFileSync } from "fs";
/** nodejs 文件操作 */
declare const fileUtils: (ROOT_PATH?: string) => {
    /** 检查文件是否存在
     * @param {string} filePath 文件路径
     * @example checkFile('src/index.ts') // true or false
     */
    checkFile(filePath: string): boolean;
    /** 读取文件内容
     * @param filePath 文件路径
     * @param isCreate 是否创建文件
     * @param option 读取配置
     * @example getFile('src/index.ts') // 'any...'
     */
    getFile(filePath: string, isCreate?: boolean, option?: Parameters<typeof readFileSync>[1]): any;
    /** 写入文件 */
    writeFile(file: import("fs").PathOrFileDescriptor, data: string | NodeJS.ArrayBufferView, options?: import("fs").WriteFileOptions | undefined): void;
    /** 头部写入文件
     * @param filePath
     * @param content
     */
    shiftFile(filePath: string, content: string): void;
    /** 尾部写入文件
     * @param filePath 文件路径
     * @param content 内容
     */
    appendFile(filePath: string, content: string): void;
    /** 删除文件
     * @type {typeof rmSync}
     */
    deleteFile(path: import("fs").PathLike, options?: import("fs").RmOptions | undefined): void;
    /** 获取文件根目录
     * @param filePath 文件路径
     */
    getFileRootPath(filePath: string): string;
    /** 复制文件
     * @type {typeof copyFileSync}
     */
    copyFileSync(src: import("fs").PathLike, dest: import("fs").PathLike, mode?: number | undefined): void;
};
export default fileUtils;
