import { copyFileSync, existsSync, readFileSync, rmSync, writeFileSync } from "fs";
import path from "path";

/** nodejs 文件操作 */
const fileUtils = (ROOT_PATH = path.resolve('./')) => ({
  /** 检查文件是否存在
   * @param {string} filePath 文件路径
   * @example checkFile('src/index.ts') // true or false
   */
  checkFile(filePath) {
    return existsSync(path.resolve(`${ROOT_PATH}${filePath}`));
  },
  /** 读取文件内容
   * @param {string} filePath 文件路径
   * @param {boolean} [isCreate=false] 是否创建文件
   * @param {Parameters<typeof readFileSync>[1]} option 读取配置
   * @returns {any}
   * @example getFile('src/index.ts') // 'any...'
   */
  getFile(filePath, isCreate = false, option = 'utf-8') {
    const file = path.resolve(`${ROOT_PATH}${filePath}`);
    const fileType = filePath.split('.').pop() || '';

    if (existsSync(file)) {
      const res = readFileSync(file, option);

      if (/json/.test(fileType)) return JSON.parse(res);
      else return res;
    } else {
      if (isCreate) return this.writeFile(filePath, '');
      else throw new Error(`文件不存在：${filePath}`);
    }
  },
  /** 写入文件
   * @param {Parameters<typeof writeFileSync>} args 内容及配置
   */
  writeFile(...args) {
    const filePath = args[0]
    const file = path.resolve(`${ROOT_PATH}${filePath}`);

    writeFileSync(file, ...args.slice(1));
  },
  /** 头部写入文件
   * @param {string} filePath
   * @param {string} content
   */
  shiftFile(filePath, content) {
    this.writeFile(filePath, `${content}\n${this.getFile(filePath, true)}`);
  },
  /** 尾部写入文件
   * @param {string} filePath 文件路径
   * @param {string} content 内容
   */
  appendFile(filePath, content) {
    this.writeFile(filePath, `${this.getFile(filePath, true)}\n${content}`);
  },
  /** 删除文件
   * @param {Parameters<typeof rmSync>} args
   */
  deleteFile(...args) {
    const file = path.resolve(`${ROOT_PATH}${args[0]}`)
    rmSync(file, ...args.slice(1));
  },
  /** 获取文件根目录
   * @param {string} filePath 文件路径
   */
  getFileRootPath(filePath) {
    return path.resolve(`${ROOT_PATH}${filePath}`);
  },
  /** 复制文件
   * @param {Parameters<typeof copyFileSync>} args 内容及配置
   */
  copyFileSync(...args) { return copyFileSync(...args) }
});

export default fileUtils;